from process_utils import *
# Edit file directory and filename to point to video to be cut


filedir = "C:/Users/Sophi/Desktop/Master_UK/Masterarbeit/coho0006_favpc2_AB/preconditioning/cut/"
filename = "coho0006_vi1_AB2018-05-28T11_01_22_cut.avi"

f_rate = 20.0

# specify the number of boxes in each axis in the grid
box_y = 2
box_x = 4

# Adjust after running test_cut to properly get only boxes videos
ystart = 250
yend = 975
xstart = 0
xend = 1922


# # Run to cut off non video bits
# test_cut(filedir, filename, [xstart, xend], [ystart, yend])


# This finds the dimensions of each box depending on variables above
boxes = boxes_locations([box_x, box_y], [xstart, xend], [ystart, yend])

# Use this to split the video into videos of individual boxes
split_box_videos(filedir, filename, boxes)


# I think this is to trim video if needed??
# print(trim_video(filedir, filename, frame_start=[video_start], frame_end=[video_end]))
# start=120
# end= 180

# # trim_video(filedir, filename, start_frame, stop_frame)


# Ana:
# print(trim_video(filedir, filename))