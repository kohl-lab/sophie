import numpy as np
import matplotlib.pyplot as plt


#set the following parameters:

pathway = 'C:/Users/Sophi/Desktop/Master_UK/Masterarbeit/download/motif_10/'

filename = 'motif_10_usage_box8_coho0005_favpc2_AB2018-02-06T08_18_31.npy'

plotname = 'plot_motif_10_box8'

boxplotname = 'boxplot_motif_10_box8'

data = np.load(pathway + filename)

print(data)


##plot
#
# plt.plot(data)
# plt.xlabel('motifs')
# plt.xticks(range(0,10))
# plt.ylabel('number of frames')
# plt.savefig(pathway + plotname + '.png')
# plt.show()


##bar plot

motifs = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
plt.bar(motifs, height=data)
plt.xlabel('motif')
plt.xticks(range(0,10))
plt.ylabel('number of frames')
plt.savefig(pathway + boxplotname + '.png')
plt.show()