from __future__ import absolute_import, division, print_function
import csv
from itertools import imap
from operator import itemgetter


def main():
    delimiter = ','
    with open('box1_coho0005_favpc2_AB2018-02-06T08_18_31.csv', 'rb') as input_file:
        reader = csv.reader(input_file, delimiter=delimiter)
        with open('output.csv', 'wb') as output_file:
            writer = csv.writer(output_file, delimiter=delimiter)
            writer.writerows(imap(itemgetter(0, 5, 4, 7), reader))


if __name__ == '__main__':
    main()