import numpy as np
import matplotlib.pyplot as plt

#set x_np.arange() to number of motifs

pathway = 'C:/Users/Sophi/Desktop/Master_UK/Masterarbeit/download/motif_8/'

filename_1 = 'motif_8_usage_box1_coho0005_favpc2_AB2018-02-06T08_18_31.npy'
filename_3 = 'motif_8_usage_box3_coho0005_favpc2_AB2018-02-06T08_18_31.npy'
filename_5 = 'motif_8_usage_box5_coho0005_favpc2_AB2018-02-06T09_09_46.npy'
filename_6 = 'motif_8_usage_box6_coho0005_favpc2_AB2018-02-06T09_09_46.npy'
filename_7 = 'motif_8_usage_box7_coho0005_favpc2_AB2018-02-11T09_04_57.npy'
filename_8 = 'motif_8_usage_box8_coho0005_favpc2_AB2018-02-06T08_18_31.npy'

data1 = np.load(pathway + filename_1)
data2 = np.load(pathway + filename_3)
data3 = np.load(pathway + filename_5)
data4 = np.load(pathway + filename_6)
data5 = np.load(pathway + filename_7)
data6 = np.load(pathway + filename_8)

#Barplot

x = np.arange(8)
fig, ax = plt.subplots()
ax.bar(x, height=data1, color='b', width=1/5, linewidth=0)
ax.bar(x + 1/5, height=data2, color='r', width=1/5, linewidth=0)
ax.bar(x + 2/5, height=data3, color='y', width=1/5, linewidth=0)
ax.bar(x + 3/5, height=data4, color='c', width=1/5, linewidth=0)
#ax.bar(x + 4/6, height=data5, color='k', width=1/6, linewidth=0)
ax.bar(x + 4/5, height=data6, color='g', width=1/5, linewidth=0)
ax.set_title('motif8')
ax.set_xticks(x)
ax.set_xlabel('motif')
ax.set_ylabel('number of frames')

plt.savefig(pathway + 'all_data.png')
fig.show()