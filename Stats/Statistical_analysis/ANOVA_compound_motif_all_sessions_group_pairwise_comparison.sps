﻿* Encoding: UTF-8.

DATASET ACTIVATE DataSet6.


GLM grooming_AX immobile_AX exploring_AX grooming_BY immobile_BY exploring_BY grooming_X immobile_X 
    exploring_X grooming_Y immobile_Y exploring_Y BY group
  /WSFACTOR=compound 4 Polynomial behaviour 3 Polynomial 
  /METHOD=SSTYPE(3)
  /EMMEANS=TABLES(compound*behaviour)COMPARE(compound)ADJ(SIDAK)
  /EMMEANS=TABLES(compound*behaviour)COMPARE(behaviour)ADJ(SIDAK)  
  /EMMEANS=TABLES(compound*behaviour*group)COMPARE(compound)ADJ(SIDAK)  
  /PLOT=PROFILE(compound*behaviour) TYPE=LINE ERRORBAR=SE(1) MEANREFERENCE=NO YAXIS=AUTO
  /EMMEANS=TABLES(compound) 
  /EMMEANS=TABLES(behaviour) 
  /EMMEANS=TABLES(compound*behaviour) 
  /PRINT=DESCRIPTIVE ETASQ HOMOGENEITY 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=compound behaviour compound*behaviour
  /DESIGN=group.

DATASET ACTIVATE DataSet1.
GLM grooming_AX immobile_AX exploring_AX grooming_BY immobile_BY exploring_BY grooming_X immobile_X 
    exploring_X grooming_Y immobile_Y exploring_Y BY group
  /WSFACTOR=compound 4 Polynomial motif 3 Polynomial 
  /METHOD=SSTYPE(3)
  /EMMEANS=TABLES(compound*motif)COMPARE(compound)ADJ(SIDAK)
  /EMMEANS=TABLES(compound*motif)COMPARE(motif)ADJ(SIDAK)  
  /EMMEANS=TABLES(compound*motif*group)COMPARE(compound)ADJ(SIDAK)   
  /PLOT=PROFILE(compound*motif*group compound*motif compound*group) TYPE=LINE ERRORBAR=SE(1) 
    MEANREFERENCE=NO YAXIS=AUTO
  /PRINT=DESCRIPTIVE ETASQ 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=compound motif compound*motif
  /DESIGN=group.
