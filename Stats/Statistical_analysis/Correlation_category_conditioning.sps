﻿* Encoding: UTF-8.


DATASET ACTIVATE DataSet8.

CROSSTABS
  /TABLES=category BY cond_effect precond_effect super_conditioning
  /FORMAT=AVALUE TABLES
  /STATISTICS=CHISQ CC PHI 
  /CELLS=COUNT
  /COUNT ROUND CELL.
