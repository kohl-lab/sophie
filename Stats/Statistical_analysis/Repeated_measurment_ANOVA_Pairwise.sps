﻿* Encoding: UTF-8.

DATASET ACTIVATE DataSet1.
GLM grooming_pre immobile_pre exploring_pre grooming_cond immobile_cond exploring_cond 
    grooming_test immobile_test exploring_test BY group
  /WSFACTOR=phases 3 Polynomial motif 3 Polynomial 
  /METHOD=SSTYPE(3)
  /POSTHOC=group(BONFERRONI) 
  /EMMEANS=TABLES(phases*group)COMPARE(group)ADJ(SIDAK)
  /EMMEANS=TABLES(phases*group)COMPARE(phases)ADJ(SIDAK)
  /EMMEANS=TABLES(phases*motif*group)COMPARE(group)ADJ(SIDAK)
  /PLOT=PROFILE(phases*motif*group phases*motif phases*group) TYPE=LINE ERRORBAR=SE(1) 
    MEANREFERENCE=NO YAXIS=AUTO
  /PRINT=DESCRIPTIVE ETASQ 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=phases motif phases*motif
  /DESIGN=group.
