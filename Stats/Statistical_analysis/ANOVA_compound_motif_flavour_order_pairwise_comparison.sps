﻿* Encoding: UTF-8.

DATASET ACTIVATE DataSet1.

GLM grooming_AX immobile_AX exploring_AX grooming_BY immobile_BY exploring_BY grooming_X immobile_X 
    exploring_X grooming_Y immobile_Y exploring_Y BY flavour_order
  /WSFACTOR=compound 4 Polynomial motif 3 Polynomial 
  /METHOD=SSTYPE(3)
  /EMMEANS=TABLES(compound*motif)COMPARE(compound)ADJ(SIDAK)
  /EMMEANS=TABLES(compound*motif)COMPARE(motif)ADJ(SIDAK)  
  /EMMEANS=TABLES(compound*motif*flavour_order)COMPARE(compound)ADJ(SIDAK)
  /PLOT=PROFILE(compound*motif*flavour_order compound*motif compound*flavour_order) TYPE=LINE 
    ERRORBAR=SE(1) MEANREFERENCE=NO YAXIS=AUTO
  /PRINT=DESCRIPTIVE ETASQ HOMOGENEITY 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=compound motif compound*motif
  /DESIGN=flavour_order.
