﻿* Encoding: UTF-8.

DATASET ACTIVATE DataSet1.

GLM grooming_session_7 grooming_session_8 grooming_session_13 grooming_session_14 
    immobile_session_7 immobile_session_8 immobile_session_13 immobile_session_14 exploring_session_7 
    exploring_session_8 exploring_session_13 exploring_session_14
  /WSFACTOR=behaviour 3 Polynomial compound 4 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(compound*behaviour) TYPE=LINE ERRORBAR=SE(1) MEANREFERENCE=NO YAXIS=AUTO
  /PRINT=DESCRIPTIVE ETASQ 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=behaviour compound behaviour*compound.

GLM grooming_session_7 grooming_session_8 grooming_session_13 grooming_session_14 
    immobile_session_7 immobile_session_8 immobile_session_13 immobile_session_14 exploring_session_7 
    exploring_session_8 exploring_session_13 exploring_session_14 BY category
  /WSFACTOR=behaviour 3 Polynomial compound 4 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(compound*behaviour compound*category compound*behaviour*category) TYPE=LINE 
    ERRORBAR=SE(1) MEANREFERENCE=NO YAXIS=AUTO
  /PRINT=DESCRIPTIVE ETASQ 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=behaviour compound behaviour*compound
  /DESIGN=category.

